import os
import logging
from elasticsearch.helpers import streaming_bulk
from smart_open import smart_open
from parser import processor
from es import es

BATCH_SIZE = int(os.getenv('BATCH_SIZE', 500))


def es_processor(credentials):
    for c in credentials:
        c.update({'_id': c['key']})
        yield c


def handler(event, context):
    for record in event['Records']:
        bucket = record['s3']['bucket']['name']
        key = record['s3']['object']['key']
        uri = 's3://{}/{}'.format(bucket, key)

        for ok, result in streaming_bulk(
            es,
            es_processor(processor(uri, smart_open(uri))),
            index='spycloud',
            doc_type='credential',
            chunk_size=BATCH_SIZE,
            yield_ok=False
        ):
            action, result = result.popitem()
            if not ok:
                logging.warning('FAILED action:{} result:{}'.format(action, result))
