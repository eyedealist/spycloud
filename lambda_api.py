from __future__ import print_function
import json
from elasticsearch.exceptions import NotFoundError
from es import es

INDEX = 'spycloud'
DOC_TYPE = 'credential'
PROPS = ('username', 'domain', 'email', 'password',)


def handle_get_list(event, context):
    query = event['queryStringParameters'] if event['queryStringParameters'] else {}

    try:
        offset = int(query.get('offset', 0))
        limit = int(query.get('limit', 50))
    except ValueError:
        return response({'message': 'Invalid request parameters'}, 400)

    size = limit if 0 < limit <= 100 else 100
    match = {key: value for key, value in query.viewitems() if key in PROPS}
    body = {
        'query': {'match': match} if match else {'match_all': {}}
    }

    result = es.search(index=INDEX, doc_type=DOC_TYPE, from_=offset, size=size, body=body)
    results = [item['_source'] for item in result['hits']['hits']]
    return response({
        'results': results,
        'count': len(results),
        'total': result['hits']['total'],
        'offset': offset
    })


def handle_get_item(event, context):
    key = event['pathParameters']['key']
    try:
        result = es.get(index=INDEX, doc_type=DOC_TYPE, id=key)
        return response(result['_source'])
    except NotFoundError:
        return not_found()


def handle_create(event, context):
    return not_implemented()


def handle_update(event, context):
    return not_implemented()


def handle_delete(event, context):
    return not_implemented()


def handle_search(event, context):
    return not_implemented()


def handle_get_similar(event, context):
    return not_implemented()


def not_implemented():
    return response({'message': 'Not Yet Implemented'}, 503)


def not_found():
    return response({'message': 'Not Found'}, 400)


def response(message, status_code=200):
    return {
        'statusCode': status_code,
        'body': json.dumps(message),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
    }
