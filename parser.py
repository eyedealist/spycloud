from __future__ import division
import os
import re
import base64
import logging

RE_CRED = re.compile(r'(?P<username>[\w\s!#$%&\'*+\-/=?^`{|}~.]+)@(?P<domain>[\w\s.\-]*)')


class ParseError(Exception):
    pass


def parse(s):
    s = s.strip()
    m = re.search(RE_CRED, s)

    if not m or len(m.groups()) != 2:
        raise ParseError('Unable to parse username and domain from provided input')
    
    data = m.groupdict()
    start = m.start('username')
    end = m.end('domain')

    # Validate the parsed username and domain contain all relevant characters
    if start > 0 and end < len(s):
        raise ParseError('Username and domain were parsed incorrectly')

    # Password is any text on the opposite side of the delimiter
    # Delimiter may repeat, so strip multiple occurrences
    if start > 0:
        delimiter = s[start-1:start]
        password = s[:start].rstrip(delimiter)
    else:
        delimiter = s[end:end+1]
        password = s[end:].lstrip(delimiter)

    data['password'] = password
    return data


def generate_key(data):
    return base64.urlsafe_b64encode('{}:{}'.format(data['email'], data['password']))


def normalize(data):
    # Remove extra whitespace and convert to lowercase
    data['username'] = data['username'].lower().replace(' ', '')
    data['domain'] = data['domain'].lower().replace(' ', '')

    # All records must have a username, domain, and password
    if not len(data['username']):
        raise ParseError('Record does not contain a username')
    if not len(data['domain']):
        raise ParseError('Record does not contain a domain')
    if not len(data['password']):
        raise ParseError('Record does not contain a password')

    # Form normalized email address
    data['email'] = '{}@{}'.format(data['username'], data['domain'])
    # Create a unique key
    data['key'] = generate_key(data)
    return data


def processor(filename, lines):
    logging.info('START {}'.format(filename))
    count = 0
    anomalies = 0
    for line in lines:
        count += 1
        try:
            data = normalize(parse(line))
            yield data
        except ParseError as e:
            anomalies += 1
            logging.warning('DISCARD line:{} {}'.format(count, e.message))

    accuracy = 100 - ((anomalies / count) * 100)
    logging.info('END Lines:{} Anomalies:{} Accuracy:{:.15f}%'.format(count, anomalies, accuracy))


def main():
    import argparse
    import json

    parser = argparse.ArgumentParser(description="Extract credentials from a text file and push to ElasticSearch")
    parser.add_argument('infile', help="Path to file containing credentials to extract")
    parser.add_argument('outfile', help='Output file path')
    args = parser.parse_args()

    logging.basicConfig(format='%(levelname)s %(asctime)s %(funcName)s %(message)s', level=logging.INFO)

    with open(args.infile, 'r') as infile, open(args.outfile, 'w') as outfile:
        for cred in processor(os.path.basename(infile.name), infile):
            outfile.write('{}\n'.format(json.dumps(cred)))


if __name__ == '__main__':
    main()
