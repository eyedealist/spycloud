import os
from aws_requests_auth.boto_utils import BotoAWSRequestsAuth
from elasticsearch import Elasticsearch, RequestsHttpConnection

ES_REGION = os.getenv('ES_REGION', 'us-east-2')
ES_HOST = os.getenv('ES_HOST')

aws_auth = BotoAWSRequestsAuth(
    aws_host=ES_HOST,
    aws_region=ES_REGION,
    aws_service='es'
)

es = Elasticsearch(
    host=ES_HOST,
    port=443,
    use_ssl=True,
    verify_certs=True,
    connection_class=RequestsHttpConnection
)
