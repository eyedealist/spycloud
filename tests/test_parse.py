from unittest import TestCase
import base64
from parser import parse, normalize, ParseError


class TestParseNotParsable(TestCase):
    def runTest(self):
        with self.assertRaises(ParseError):
            parse('invalid')


class TestParse(TestCase):
    def __init__(self, *args, **kwargs):
        super(TestParse, self).__init__(*args, **kwargs)
        self.expected = self.expected = {
            'username': 'username',
            'domain': 'domain.com',
            'password': 'password'
        }

    def setUp(self):
        self.data = 'username@domain.com:password'
    
    def check(self, prop):
        result = parse(self.data)
        self.assertEqual(result[prop], self.expected[prop], 'Expected `{}`, found `{}`'.format(
            self.expected[prop], result[prop]
        ))

    def test_username(self):
        self.check('username')

    def test_domain(self):
        self.check('domain')

    def test_password(self):
        self.check('password')


class TestParseCommaDelimited(TestParse):
    def setUp(self):
        self.data = 'username@domain.com,password'


class TestParseSemicolonDelimited(TestParse):
    def setUp(self):
        self.data = 'username@domain.com;password'


class TestParseTildeDelimited(TestParse):
    def setUp(self):
        self.data = 'username@domain.com~password'


class TestParsePrefixedPassword(TestParse):
    def setUp(self):
        self.data = 'password:username@domain.com'


class TestParseRepeatedDelimiters(TestParse):
    def setUp(self):
        self.data = 'username@domain.com~~~password'


class TestParseAllowsSpaces(TestParse):
    def setUp(self):
        self.data = 'user name@do main.com:password'
        self.expected.update({
            'username': 'user name',
            'domain': 'do main.com'
        })


class TestNormalizeSetsLowercase(TestCase):
    def setUp(self):
        self.data = normalize({
            'username': 'UsErNaMe',
            'domain': 'DoMaIn.Com',
            'password': 'PASSWORD'
        })

    def test_username_sets_lowercase(self):
        self.assertEqual(self.data['username'], 'username')

    def test_domain_sets_lowercase(self):
        self.assertEqual(self.data['domain'], 'domain.com')

    def test_password_remains_unchanged(self):
        self.assertEqual(self.data['password'], 'PASSWORD')


class TestNormalizeRemovesSpaces(TestCase):
    def setUp(self):
        self.data = normalize({
            'username': 'user na  me',
            'domain': 'dom ain.com',
            'password': 'pass word'
        })

    def test_username_removes_spaces(self):
        self.assertEqual(self.data['username'], 'username')

    def test_domain_removes_spaces(self):
        self.assertEqual(self.data['domain'], 'domain.com')

    def test_password_remains_unchanged(self):
        self.assertEqual(self.data['password'], 'pass word')


class TestNormalizeEmptyUsername(TestCase):
    def runTest(self):
        with self.assertRaises(ParseError):
            normalize({
                'username': '',
                'domain': 'domain.com',
                'password': 'password'
            })


class TestNormalizeEmptyDomain(TestCase):
    def runTest(self):
        with self.assertRaises(ParseError):
            normalize({
                'username': 'username',
                'domain': '',
                'password': 'password'
            })


class TestNormalizeEmptyPassword(TestCase):
    def runTest(self):
        with self.assertRaises(ParseError):
            normalize({
                'username': 'username',
                'domain': 'domain.com',
                'password': ''
            })


class TestNormalizeFields(TestCase):
    def setUp(self):
        self.data = normalize({
            'username': 'username',
            'domain': 'domain.com',
            'password': 'password'
        })

    def test_has_email(self):
        self.assertIn('email', self.data)

    def test_email_formed_correctly(self):
        self.assertEqual('username@domain.com', self.data['email'])

    def test_has_key(self):
        self.assertIn('key', self.data)

    def test_key_base64_encoded(self):
        self.assertEqual('username@domain.com:password', base64.urlsafe_b64decode(self.data['key']))
